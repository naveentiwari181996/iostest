
package com.partronapp;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.partron.wearable.band.sdk.core.BandResultCode;
import com.partron.wearable.band.sdk.core.BandUUID;
import com.partron.wearable.band.sdk.core.PWB_ClientManager;
import com.partron.wearable.band.sdk.core.UserProfile;
import com.partron.wearable.band.sdk.core.interfaces.BandScanCallback;
import com.partron.wearable.band.sdk.core.interfaces.OnCompleteListener;
import com.partron.wearable.band.sdk.core.interfaces.PWB_Client;

import java.util.ArrayList;

import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;


public class PartronSDKModule extends ReactContextBaseJavaModule implements BandScanCallback {
    private LeDeviceListAdapter mLeDeviceListAdapter;
  private final ReactApplicationContext reactContext;
  public static PWB_Client mClient;

  public PartronSDKModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
      init();
  }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    @Override
    public void onBandScanCallback(int state, final BluetoothDevice bluetoothDevice, int rssi) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                WritableMap params = Arguments.createMap();
                if(bluetoothDevice == null){
                    params.putString("BandSearchStatus","Device not found");
                    sendEvent(reactContext, "BandConnectionEvent", params);
                    return;
                }
                params.putString("BandSearchStatus", "Device found");
                sendEvent(reactContext, "BandConnectionEvent", params);
                mClient.bandConnect(bluetoothDevice.getAddress());
                mLeDeviceListAdapter.addDevice(bluetoothDevice);
            }
        });
    }

    private void init() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                UserProfile item = new UserProfile();
                item.setAge(31);
                item.setHeight(170);
                item.setWeight(70);
                item.setGender(0);
                PWB_ClientManager clientManager = PWB_ClientManager.getInstance();
                mClient = clientManager.create(getReactApplicationContext(), item, BandUUID.PWB_250);
            }
        });
    }

    @ReactMethod
    public void connectBand(){
      if(mClient != null) {
          mClient.registerBandScanCallback(this);
          mClient.bandScan().start(1000 * 5);
      }
    }

    @ReactMethod
    public void stopBandScan(){
        if (mClient != null)
            mClient.bandScan().stop();
    }

    @ReactMethod
    public void isBandConnected(final Callback successCallback
                               ){
        boolean isConnected = false;
        if(mClient != null) {
            isConnected = mClient.isBandConnected();
        }
         successCallback.invoke(isConnected?"Band is connected":"Band is not connected");
    }

    @ReactMethod
    public void setUrbanGoalStep(final Callback successCallback,
                                 final Callback errorCallback) {
        if (mClient != null) {
            int step = 10000;
            mClient.getUrbanMode().setUrbanGoalStep(step, new OnCompleteListener() {
                @Override
                public void onResult(int result, Object o) {
                    String message = "Band result code is ";
                    //Handle BandResultCode, Object null
                    if (result == BandResultCode.SUCCESS) {
                        successCallback.invoke(message + getBandStatusString(result));
                    } else {
                        errorCallback.invoke(message + getBandStatusString(result));
                    }
                }
            });
        }
        else{
            errorCallback.invoke("Client is null");
        }
    }

    @ReactMethod
    public void bandDisconnect() {
        if (mClient != null) mClient.bandDisconnect();
    }


    private String getBandStatusString(int statusCode){
      String statusDetails = "";
        switch (statusCode){
            case BandResultCode.FAIL: statusDetails = "FAIL";break;
            case BandResultCode.BAND_CHARGING: statusDetails = "BAND_CHARGING";break;
            case BandResultCode.BAND_CONNECTION_FAIL: statusDetails = "BAND_CONNECTION_FAIL";break;
            case BandResultCode.BAND_DISCONNECD: statusDetails = "BAND_DISCONNECT";break;
            case BandResultCode.BAND_FIRMWARE_UPDATING: statusDetails = "BAND_FIRMWARE_UPDATING";break;
            case BandResultCode.BAND_NO_SYNC_DATA: statusDetails = "BAND_NO_SYNC_DATA";break;
            case BandResultCode.BAND_SCAN_TIMEOUT: statusDetails = "BAND_SCAN_TIMEOUT";break;
            case BandResultCode.BAND_SYNCING: statusDetails = "BAND_SYNCING";break;
            case BandResultCode.BLUETOOTH_DISABLE: statusDetails = "BLUETOOTH_DISABLE";break;
            case BandResultCode.REQUEST_TIMEOUT_ERROR: statusDetails = "REQUEST_TIMEOUT_ERROR";break;
            case BandResultCode.SUCCESS: statusDetails = "SUCCESS";
        }
        return statusDetails;
    }
  @Override
  public String getName() {
    return "PartronModule";
  }

    private class LeDeviceListAdapter {
        private ArrayList<BluetoothDevice> mLeDevices;
        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<BluetoothDevice>();
        }
        public void addDevice(BluetoothDevice device) {
            if(!mLeDevices.contains(device)) {
                mLeDevices.add(device);
            }
        }
        public BluetoothDevice getDevice(int position) {
            return mLeDevices.get(position);
        }
        public void clear() {
            mLeDevices.clear();
        }
    }
}