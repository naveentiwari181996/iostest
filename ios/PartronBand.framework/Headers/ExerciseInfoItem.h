//
//  ExerciseInfoItem.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExerciseInfoItem : NSObject<NSCoding>

@property (nonatomic, strong) NSDate *startDate, *stopDate;
@property NSUInteger step;
@property float distance;
@property NSUInteger calorie;
@property float speed;
@property NSUInteger time;
@property NSUInteger restTime;
@property NSUInteger altitude;
@property NSUInteger maxAltitude;
@property NSUInteger ppg;
@property NSUInteger maxPPG;
@property BOOL band;

@end
