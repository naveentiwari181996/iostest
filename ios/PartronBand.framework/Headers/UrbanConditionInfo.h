//
//  UrbanConditionInfo.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UrbanConditionInfo : NSObject
    @property NSUInteger condition;
    @property NSUInteger sleep;
    @property NSUInteger sleepTime;
    @property NSUInteger deepSleepTime;
    @property NSUInteger lightSleepTime;
    @property NSUInteger recover;
    @property NSUInteger stress;


@end
