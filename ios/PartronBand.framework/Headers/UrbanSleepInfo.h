//
//  UrbanSleepInfo.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UrbanSleepInfo : NSObject

@property  (nonatomic,strong) NSDate *startDate;
@property NSUInteger totalTime;
@property NSUInteger hourDeep;
@property NSUInteger hourLight;
@property NSUInteger totalDeep;
@property NSUInteger totalLight;
@property NSDate *date;

@end
