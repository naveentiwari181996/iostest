//
//  PWBUserProfile.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWBUserProfile : NSObject

    @property NSUInteger age;
    @property NSUInteger height;
    @property NSUInteger weight;
    @property NSUInteger gender;

@end
