//
//  PWBSerialPacket.h
//  PWB200SDK
//
//  Created by TSup on 2016. 4. 26..
//  Copyright © 2016년 Partron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PWBPacket.h"

@interface PWBSerialPacket : NSObject
{
    NSMutableDictionary *packets;
}
@property NSInteger tag;

- (NSData *)data;
- (NSUInteger)length;
- (PWBPacket*)checksum;

- (instancetype)initWithCapacity:(NSUInteger)capacity;
- (void)setPacket:(PWBPacket*)packet packetSequence:(NSUInteger)packetSequence;
- (PWBPacket*)packetSequence:(NSUInteger)packetSequence;

@end
