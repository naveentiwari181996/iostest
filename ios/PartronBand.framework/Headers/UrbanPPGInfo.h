//
//  UrbanPPGInfo.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UrbanPPGInfo : NSObject

@property NSUInteger hrm;
@property NSUInteger stress;
@property NSUInteger hour;
@property NSUInteger min;
@property NSDate *date;

@end
