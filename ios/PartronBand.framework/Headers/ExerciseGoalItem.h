//
//  ExerciseGoalItem.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface ExerciseGoalItem : NSObject

    @property NSUInteger mode;
    @property NSUInteger step;
    @property NSUInteger distance;
    @property NSUInteger time;
    @property NSUInteger altitude;

@end
