//
//  PWBPacket.h
//  PWB200SDK
//
//  Created by TSup on 2016. 4. 26..
//  Copyright © 2016년 Partron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PWBPacket : NSObject

@property (nonatomic, strong, nullable) NSData *data;
@property NSUInteger length;

- (nullable instancetype)initWithData:(nullable NSData*)data;
- (nullable instancetype)initWithBytes:(nullable const void*)bytes;

+ (nullable PWBPacket*)packetWithData:(nullable NSData *)data;
+ (nullable PWBPacket*)packetWithInteger:(NSUInteger)num;
+ (nullable PWBPacket*)packetWithBytes:(nullable const void*)bytes;
+ (nullable PWBPacket*)packetWithString:(nullable NSString*)str;

@end
