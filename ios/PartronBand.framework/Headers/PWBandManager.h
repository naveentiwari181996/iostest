//
//  PWBandManager.h
//  PWB200SDK
//
//  Created by TSup on 2016. 4. 26..
//  Copyright © 2016년 Partron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "PWBSerialPacket.h"
#import "PWBObject.h"

typedef NS_ENUM(NSInteger, PWBConnectionState) {
    PWBConnectionStateDisable,
    PWBConnectionStateDisconnected,
    PWBConnectionStateConnecting,
    PWBConnectionStateConnected,
    PWBConnectionStateConnectFailed,
    PWBConnectionStateDisconnecting,
    PWBConnectionStateScanning,
    PWBConnectionStateScanningTimeout
};

typedef NS_ENUM(NSInteger, ExerciseMode) {
    ExerciseModeWalk,
    ExerciseModeRun,
    ExerciseModeClimb,
    ExerciseModeBike
};
typedef NS_ENUM(NSInteger, ExerciseStatus) {
    ExerciseStatusNotExercise,
    ExerciseStatusExercising
};

typedef NS_ENUM(NSInteger, UpdateStatus) {
    UpdateStatusDisable,
//    UpdateStatusDownloading,
    UpdateStatusInstalling,
    UpdateStatusFinished
};


typedef NS_ENUM(NSInteger, PWBLanguage) {
    PWBLanguageKorean,
    PWBLanguageEnglish,
    PWBLanguageEtc
};

typedef NS_ENUM(NSInteger, PWBUserGender) {
    PWBUserGenderMale,
    PWBUserGenderFemale
};

typedef NS_ENUM(NSInteger, BandModel) {
    PWB100,
    PWB200,
    PWB250
};


@protocol PWBandManagerConnectDelegate;
@protocol PWBandManagerUrbanDelegate;
@protocol PWBandManagerExerciseDelegate;
@protocol PWBandManagerNotificationDelegate;
@protocol PWBandManagerTestDelegate;

@interface PWBandManager : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate,NSURLSessionDelegate>

@property (weak) id<PWBandManagerConnectDelegate>connectDelegate;
@property (weak) id<PWBandManagerUrbanDelegate>urbanDelegate;
@property (weak) id<PWBandManagerExerciseDelegate>exerciseDelegate;
@property (weak) id<PWBandManagerNotificationDelegate>notificationDelegate;
@property (weak) id<PWBandManagerTestDelegate>testDelegate;
@property PWBConnectionState state;

+(PWBandManager *)sharedInstance;
-(void)manageWithDelegate:(id<PWBandManagerConnectDelegate>)connecDelegate userProfile:(PWBUserProfile*)profile bandModel:(BandModel) model;
-(void)setConnectDelegate:(id<PWBandManagerConnectDelegate>)connectDelegate;
-(void)setUrbanDelegate:(id<PWBandManagerUrbanDelegate>)urbanDelegate;
-(void)setExerciseDelegate:(id<PWBandManagerExerciseDelegate>)exerciseDelegate;
-(void)setNotificationDelegate:(id<PWBandManagerNotificationDelegate>)notificationDelegate;
-(void)setTestDelegate:(id<PWBandManagerTestDelegate>)testDelegate;

#pragma mark - BLE Connection

-(void)connectBand:(CBPeripheral*)device;
-(void)disconnectBand;
-(PWBConnectionState)getConnectionState;
-(BOOL)isBandConnected;
-(BOOL)isExercising;
-(void)startBandScan:(NSTimeInterval)time;
//-(void)bandAutomaticConnect:(NSString*)deviceID completion:(void(^)(PWBConnectionState state))handler;
-(void)stopBandScan;
-(void)connectBandWithDeviceID:(NSString *)deviceID;
-(NSArray<CBPeripheral*>*)getPairedDeviceList;

-(NSString*)getDeviceUUID;

#pragma mark - Urban
-(void)requestPpgMeasure:(void(^)(BOOL measuring,NSInteger ppg,NSInteger stress,NSError *error))handler;
-(void)requestAltitudeMeasureWithPressure:(CGFloat)pressure completion:(void(^)(NSInteger alt,NSError *error))handler;
-(void)requestThermometer:(void(^)(BOOL measuring, NSNumber* thermo, NSError *error))handler;
-(void)requestSwing:(void(^)(NSNumber* speed, NSNumber* headSpeed, NSError *error))handler;

-(void)stopPPGMeasure:(void(^)(NSError *error))handler;;
-(void)stopAltitudeMeasure:(void(^)(NSError *error))handler;;
-(void)stopThermometerMeasure:(void(^)(NSError *error))handler;
-(void)stopSwingMeasure:(void(^)(NSError *error))handler;

-(void)setUrbanGoalStep:(NSUInteger)stepGoal completion:(void(^)(NSError *error))handler;
-(void)requestUrbanInfoWithCompletion:(void(^)(UrbanPedoInfo *info, NSError *error))handler;
-(void)requestUrbanInfoSyncWithCompletion:(void(^)(NSArray *info,NSError *error))handler;
-(void)requestUrbanPPGInfoSyncWithCompletion:(void(^)(NSArray *info,NSError *error))handler;
-(void)requestUrbanSleepInfoSyncWithCompletion:(void(^)(NSArray *info,NSError *error))handler;
-(void)requestRealTimePedoInfo:(BOOL)urban;
-(void)requestSleepingWithCompletion:(void(^)(BOOL isSleep,NSError*error))handler;

#pragma mark - Exercise
-(void)requestExerciseStart:(ExerciseGoalItem*)goalItem seaLevelPressure:(float)slp completion:(void(^)(NSError *error))handler;
-(void)requestExerciseStopWithCompletion:(void(^)(ExerciseInfoItem *item, NSError *error))handler;
-(void)requestExerciseSyncMode:(ExerciseMode)exMode completion:(void(^)(ExerciseInfoItem *item,NSArray*array, NSError *error))handler;
-(void)requestExercisingSyncMode:(ExerciseMode)exMode completion:(void(^)(ExerciseInfoItem *item, NSError *error))handler;
-(void)requestExerciseStatusWithCompletion:(void(^)(ExerciseMode mode,ExerciseStatus status,NSError*error))handler;
-(void)requestExercisePPGStart:(void(^)(NSError *error))handler;
-(void)confirmExerciseStart:(ExerciseGoalItem*)gItem seaLevelPressure:(float)slp;

#pragma mark - Setting
-(void)setUserProfile:(PWBUserProfile*)profile completion:(void(^)(NSError *error))handler;
-(void)setRTC:(void(^)(NSError *error))handler;
-(void)setLanguage:(PWBLanguage)language completion:(void(^)(NSError *error))handler;
-(void)setBandUnit:(PWBUnitItem*)unit  completion:(void(^)(NSError *error))handler;
-(void)getFirmwareVersionCompletion:(void(^)(NSString *version,NSInteger type, NSError *error))handler;
-(void)setWarningHeartRate:(NSUInteger)hrm alert:(BOOL)alarm completion:(void(^)(NSError *error))handler;
-(void)setExerciseDisplay:(ExerciseDisplay*)display completion:(void(^)(NSError *error))handler;
-(void)setExerciseBandDisplay:(ExerciseDisplay*)display completion:(void(^)(NSError *error))handler;
-(void)setSleepTimeSetting:(NSInteger)starTime to:(NSInteger)endTime completion:(void(^)(NSError *error))handler;
-(void)setExercisePPGIntervalRealTime:(BOOL)realTime completion:(void(^)(NSError *error))handler;
-(void)setUrbanStepIntervalSetting:(BOOL)isRealTime completion:(void(^)(NSError *error))handler;
-(void)setDisconnectAlarm:(BOOL)alarm completion:(void(^)(NSError *error))handler;
-(void)setTilt:(BOOL)alarm completion:(void(^)(NSError *error))handler;
-(void)setUrbanPPGInterval:(NSInteger)interval completion:(void(^)(NSError *error))handler;
-(void)setBandDisplayOrientation:(NSInteger) display completion:(void(^)(NSError *error))handler;
-(void)setSleepAlarm:(BOOL)isAlarm alramTime:(NSInteger)time completion:(void(^)(NSError *error))handler;
-(void)setTimeFormat:(NSInteger) timeFormat completion:(void(^)(NSError *error))handler;

#pragma mark - Notification
-(void)notiCallRingingCompletion:(void(^)(NSError *error))handler;
-(void)notiCallOffHookCompletion:(void(^)(NSError *error))handler;
-(void)notiGoalSuccessCompletion:(void(^)(NSError *error))handler;
-(void)requestBatteryInfo:(void(^)(NSInteger level,NSError*error))handler;
-(void) setANCSNotification:(NSInteger)notiType isNotiArrays:(NSMutableArray*)arrays completion:(void (^)(NSError *error))handler;

#pragma mark - CUSTOM
-(void)setCustomContentsWithDuration:(NSInteger)time vibrate:(BOOL)vibrate storageType:(BOOL)storageType data:(NSData*)data completion:(void(^)(NSError *error))handler;

#pragma mark - OTA
-(void)updateBandFirmwareVersion:(NSMutableData*)data completion:(void(^)(UpdateStatus status, float progressRate, NSError* error))handler;
@end


@class PWBandManager;

#pragma mark -
#pragma mark - PWB DELEGATE

@protocol PWBandManagerConnectDelegate <NSObject>
@required
-(void)bandManagerDidUpdateState:(PWBConnectionState)state;
-(void)bandManagerDidFinishScanDevices:(NSDictionary*)devicesInfo;
@end

@protocol PWBandManagerUrbanDelegate <NSObject>
@required
#pragma mark - Urban
-(void)bandUrbanPedometerInfoDidUpdate:(UrbanPedoInfo*)info db:(BOOL)save;
-(void)bandUrbanInfoDidUpdate:(UrbanPedoInfo*)info;
-(void)bandUrbanPPGInfoDidUpdate:(UrbanPPGInfo*)info db:(BOOL)save;
-(void)bandUrbanConditionInfoDidUpdate:(UrbanConditionInfo*)info;
-(void)bandUrbanSleepDidUpdate:(UrbanSleepInfo*)info finished:(BOOL)finished;
-(void)bandBatteryInfoUpdate:(NSInteger)level;
@end

@protocol PWBandManagerExerciseDelegate <NSObject>
@required
#pragma mark - Exercise
-(void)bandDidStartExercise;
-(void)bandDidStopExercise:(ExerciseInfoItem*)info;
-(void)bandExerciseInfoDidUpdate:(ExerciseInfoItem*)info;
-(void)bandExerciseDBInfoDidUpdate:(ExerciseSyncDBItem*)info;
-(void)bandExercisePPGDidUpdate:(NSUInteger)hrm;
-(void)bandExerciseRealtimeInfoDidUpdate:(ExerciseInfoItem*)info;


//-(void)bandTest:(NSString*)str message:(NSString*)message;
@end

@protocol PWBandManagerNotificationDelegate <NSObject>
@required
#pragma mark - Notification
-(void)bandDidGoalNotification:(int) mode;
-(void)bandANCSNotificationDidUpdate:(NSArray<NSNumber*>*)info;
@end


@protocol PWBandManagerTestDelegate <NSObject>
@required
#pragma mark - TEST
-(void)bandTest:(NSString*)str message:(NSString*)message;
@end
