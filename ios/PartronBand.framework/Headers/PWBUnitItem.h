//
//  PWBUnitItem.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PWBDistanceUnit) {
    PWBDistanceUnitKM,
    PWBDistanceUnitMile
};

typedef NS_ENUM(NSInteger, PWBSpeedUnit) {
    PWBSpeedUnitKMH,
    PWBSpeedUnitMPH
};

typedef NS_ENUM(NSInteger, PWBAltitudeUnit) {
    PWBAltitudeUnitMeter,
    PWBAltitudeUnitFeet
};

@interface PWBUnitItem : NSObject

    @property PWBDistanceUnit distance;
    @property PWBSpeedUnit speed;
    @property PWBAltitudeUnit altitude;

@end
