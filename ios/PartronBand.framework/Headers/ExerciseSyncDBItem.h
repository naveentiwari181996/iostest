//
//  ExerciseSyncDBItem.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExerciseSyncDBItem : NSObject

    @property NSUInteger time;
    @property NSUInteger hrm;
   @property  NSUInteger altitude;

@end
