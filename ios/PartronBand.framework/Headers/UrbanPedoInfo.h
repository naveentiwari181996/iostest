//
//  UrbanPedoInfo.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UrbanPedoInfo : NSObject

@property NSUInteger hour;
@property NSUInteger min;
@property NSUInteger step;
@property float distance;
@property NSUInteger calorie;
@property NSUInteger hrm;
@property NSUInteger stress;
@property NSDate *date;



@end
