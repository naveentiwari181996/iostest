//
//  ExerciseDisplay.h
//  PartronBand
//
//  Created by TSup on 2016. 5. 18..
//  Copyright © 2016년 hiveit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExerciseDisplay : NSObject

    @property BOOL hrm;
    @property BOOL time;
    @property BOOL step;
    @property BOOL distance;
    @property BOOL calorie;
    @property BOOL speed;
    @property BOOL altitude;

@end
