// CalendarManager.m
#import "PartronSDKManager.h"
#import <PartronBand/PartronBand.h>

@implementation PartronSDKManager
RCT_EXPORT_MODULE(PartronModule);

-(instancetype)init{
  if( self = [super init] )
  {
    deviceList = [[NSMutableArray alloc]init];
    deviceSet = [[NSMutableSet alloc]init];
    
    
    //*****************************Delegate register & initialize ************************************//
    PWBUserProfile *user = [[PWBUserProfile alloc]init];
    [user setAge:25];
    [user setGender:PWBUserGenderMale];
    [user setHeight:170];
    [user setWeight:50];
    
    [[PWBandManager sharedInstance]manageWithDelegate:self userProfile:user bandModel:PWB200];
    
    [[PWBandManager sharedInstance]setConnectDelegate:self];
    [[PWBandManager sharedInstance]setUrbanDelegate:self];
    [[PWBandManager sharedInstance]setUrbanDelegate:self];
  }
  return self;
}

- (NSArray<NSString *> *)supportedEvents
{
  return @[@"BandConnectionEvent"];
}

//************************Band Scans Delegate**********************//
-(void)bandManagerDidFinishScanDevices:(NSDictionary *)devicesInfo{
  NSString *local_name = [devicesInfo valueForKey:@"local_name"];
  if(!local_name){
    [self sendEventWithName:@"BandConnectionEvent" body:@{@"BandSearchStatus": @"Device not found"}];
    return;
  }
  
  if(deviceSet && [deviceSet count] > 0){
    NSArray *arrays = [deviceSet allObjects];
    for (NSDictionary *dic in arrays) {
      CBPeripheral *device = [dic valueForKey:@"device"];
      CBPeripheral *scanDevice = [devicesInfo valueForKey:@"device"];
      if([device.identifier.UUIDString isEqualToString:scanDevice.identifier.UUIDString]){
        return;
      }
    }
  }
  
  [deviceSet addObject:devicesInfo];
  NSArray *arrays = [[NSArray alloc]init];
  NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"ad" ascending:NO];
  arrays = [deviceSet sortedArrayUsingDescriptors:@[descriptor]];
  deviceList = [arrays copy];
  NSDictionary *testConnDic = [deviceList objectAtIndex:0];
  CBPeripheral *testDevice = [testConnDic valueForKey:@"device"];
  [[PWBandManager sharedInstance]connectBand:testDevice];
  [self sendEventWithName:@"BandConnectionEvent" body:@{@"BandSearchStatus": @"Device found"}];
  //Band Connect
}

RCT_EXPORT_METHOD(connectBand){
  if(![[PWBandManager sharedInstance]isBandConnected]){
    //************************Band Scans**********************//
    [[PWBandManager sharedInstance] startBandScan:5];
    //*************************************************************//
  }
}

RCT_EXPORT_METHOD(stopBandScan){
  [[PWBandManager sharedInstance]stopBandScan];
}

RCT_EXPORT_METHOD(isBandConnected:(RCTResponseSenderBlock)successcallback) {
  Boolean connectionStatus = [[PWBandManager sharedInstance]isBandConnected];
  NSArray *events = @[connectionStatus ? @"Band is connected" : @"Band is not connected"];
  successcallback(events);
}

RCT_EXPORT_METHOD(setUrbanGoalStep:(RCTResponseSenderBlock)successcallback errorCallback:(RCTResponseErrorBlock)errorCallback){
 NSUInteger step = 10000; [[PWBandManager sharedInstance]setUrbanGoalStep: step completion:^(NSError *error){                 if (!error) {                     //Your code...
   NSArray *events = @[@"Successful"];
   successcallback(events);
 }
 else{
   errorCallback(error);
 }
 }];
  
}

RCT_EXPORT_METHOD(bandDisconnect){
  [[PWBandManager sharedInstance]disconnectBand];
}

@end
