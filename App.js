import React, { Component } from 'react';
import PartronModule from './PartronModule'
import { StyleSheet, Text, Button, View, NativeEventEmitter,NativeModules,ActivityIndicator } from 'react-native';
const partronModuleEventEmitter = new NativeEventEmitter(PartronModule);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchingForBand: false,
    }
  }

  componentWillMount() {
    partronModuleEventEmitter.addListener('BandConnectionEvent', this.bandConnectEvent);
  }

  componentDidMount() {

  }

  bandConnectEvent = (e) =>{
    this.setState({ searchingForBand: false })
    alert(JSON.stringify(e))
  }

  setUrbanGoalStep = () => {
    PartronModule.setUrbanGoalStep((message) => {
      alert(message)
    }, (errorDetails) => {
      alert(errorDetails)
    });
  }

  connectToBand = () => {
    this.setState({ searchingForBand: true })
    PartronModule.connectBand();
  }

  checkBandConnectionStatus = () =>{
    PartronModule.isBandConnected((connectionStatus) => {
      alert(connectionStatus)
    });
  }

  disconnectBand = () => {
    PartronModule.bandDisconnect();
    alert("Band Disconnected")
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Sample App</Text>
        <View style={styles.buttonContainer}>
          <Button onPress={this.connectToBand} title="Connect to band" color="#841584" />
        </View>
        <View style={styles.buttonContainer}>
          <Button onPress={this.setUrbanGoalStep} title="Set Urban Goal Step" color="#841584" />
        </View>
        <View style={styles.buttonContainer}>
          <Button onPress={this.checkBandConnectionStatus} title="Band Connection Status" color="#841584" />
        </View>
        <View style={styles.buttonContainer}>
          <Button onPress={this.disconnectBand} title="Disconnect Band" color="#841584" />
        </View>
        <View>
          <Text>Connect to band will search for available band for 5 seconds</Text>
        </View>
        {this.state.searchingForBand ?
          <View>
            <ActivityIndicator size="small" color="#841584" />
            <Text>Searching for band. Please Wait</Text>
          </View> : <View></View>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  buttonContainer: {
    margin: 10,
    margin: 10
  }
});
